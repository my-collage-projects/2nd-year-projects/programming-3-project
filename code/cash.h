#ifndef CASH_H
#define CASH_H
#include "bill.h"


class cash : public bill
{
    private:
        float after_discount;
    public:
        cash();
        cash(float t,int b_n,int s_n,string name,date b_d,pnod* phead);
        void write_bill_on_file();
        void print_bill();
        int get_cost();
        ~cash();

};

#endif // CASH_H
