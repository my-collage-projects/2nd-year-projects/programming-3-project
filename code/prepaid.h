#ifndef PREPAID_H
#define PREPAID_H

#include "bill.h"


class prepaid : public bill
{
    public:
        prepaid();
        prepaid(float t,int b_n,int s_n,string name,date b_d,pnod* phead);
        void write_bill_on_file();
        void print_bill();
        int get_cost();
        ~prepaid();

};

#endif // PREPAID_H
