#include "cash.h"
#include<iostream>
#include<string.h>
using namespace std;
#include "bill.h"

bill::bill()
{
    s_num = 0;
    c_name = " ";
    b_num = 0;
    head = NULL;
    b_date.d=0;
    b_date.m=0;
    b_date.y=0;
    total=0;
    after_discount = 0;
}

bill::bill(float t,int b_n,int s_n,string name,date b_d,pnod* phead)
{
    s_num = s_n;
    b_num = b_n;
    c_name = name;
    b_date = b_d;
    head = phead;
    total = t;
}

void bill::write_bill_on_file()
{
    ofstream bill_file;
    bill_file.open("bills.txt" , ios::app);
    bill_file << "\n" << b_num << "  " << c_name << "  " << s_num << "  " << b_date.d << "/" << b_date.m << "/" << b_date.y << " \t";
    bill_file.close();
}

int bill::get_bill_num()
{
    return b_num;
}

void bill::add_product_to_bill(pnod* pnew)
{
    if(head == NULL)
    {
        head =pnew;
        head->next = NULL;
    }
    else if ((head->pnum) > (pnew->pnum))
    {
        pnew->next = head;
        head = pnew;
    }
    else
    {
        pnod* prev = head;
        pnod* curr = head->next;
        while((curr!=NULL) && (curr->pnum < pnew->pnum))
        {
            prev = prev->next;
            curr = curr->next;
        }
        pnew->next = curr;
        prev->next = pnew;
    }
}

date* bill::get_bill_date()
{
    return &b_date;
}

int bill::get_cost()
{

}

void bill::print_bill()
{
    cout << "|   "<< b_num  << "    .  " << c_name << "   .  " << s_num << "   .  " << b_date.d << "/" << b_date.m << "/"<< b_date.y ;
}

bill::~bill()
{
    delete &s_num;
    delete &c_name;
    delete &b_num;
    delete &b_date;
    delete &total;
    delete &after_discount;
    while ( head!= NULL)
    {
        delete head;
        head = head->next;
    }
}
