#include "expire_product.h"
using namespace std;
#include"product.h"
#include<iostream>
#include<fstream>

expire_product::expire_product()
{
    d.d=0;
    d.m=0;
    d.y=0;
}

expire_product::expire_product(date dat,int n,string name,int pn ,int b_price ,int b_pieces) : product(n,name,pn,b_price,b_pieces)
{
    d.d=dat.d;
    d.m=dat.m;
    d.y=dat.y;
}

void expire_product::write_product_on_file()
{
    product::write_product_on_file();
    ofstream product_file;
    product_file.open("product.txt",ios::app);
    product_file << "\t" << d.d << "/" << d.m << "/" << d.y  << "\n";
    product_file.close();

}

date* expire_product::get_expiry_date()
{
    return &d;
}

void expire_product::print()
{
    product ::print();
    cout << "(" << d.d << "/" << d.m << "/" << d.y << ")  ||";
}

expire_product::~expire_product()
{
    delete &d;
}


