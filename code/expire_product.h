#ifndef EXPIRE_PRODUCT_H
#define EXPIRE_PRODUCT_H
#include "product.h"


class expire_product: public product
{
    private:
        date d;
    public:
        expire_product();
        expire_product(date d,int n,string name,int pn , int b_price , int b_pieces);
        void print();
        void write_product_on_file();
        date* get_expiry_date();
        ~expire_product();

};

#endif // HAS_DATE_H
