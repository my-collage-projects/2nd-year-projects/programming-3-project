#ifndef PRODUCT_H
#define PRODUCT_H
#include <iostream>
using namespace std;

struct date
{
    int d,m,y;
};

class product
{
protected:
    int p_num;
    int l_num,box_price,box_pieces;
    string p_name;
public:
        product();
        ~product();
        product(int n,string name,int pn , int b_price , int b_pieces);
        virtual void print();
        int get_product_num();
        int get_box_price();
        virtual date* get_expiry_date();
        virtual void write_product_on_file();
};

#endif // PRODUCT_H
