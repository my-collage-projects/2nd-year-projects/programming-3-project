#ifndef STORE_H
#define STORE_H
#include "product.h"
#include<fstream>
#include<string.h>

struct p_n
{
    int num;
    int quantity;
    struct p_n *next;
};

class store
{
private:
    int s_num;
    string s_add;
    p_n* head;
public:
    store();
    store(int n,string add,p_n* );
    ~store();
    int quantity_add(int n, int m);
    bool product_exist(int n);
    int reload_products();
    void print_s();
    int get_num();
    int get_quantity(int n);
    int need_to_reload(int n);
    p_n* get_head();
    void write_on_file();
    void add_product(p_n* p);
    void delete_product(int n);
};

#endif // STORE_H
