#include<iostream>
using namespace std;
#include"store.h"
#include"product.h"
#include"expire_product.h"
#include"unxpire_product.h"
#include "bill.h"
#include "cash.h"
#include "prepaid.h"

struct b_node
{
    bill* bptr;
    b_node* next;
};

struct p_node
{
    product* ptr;
    p_node* next;
};

struct s_node
{
    store* sptr;
    s_node* next;
};

int p_numb,s_numb,b_numb;
s_node* s_head=NULL;
p_node* p_head=NULL;
b_node* b_head=NULL;
date curr;

void write_product();
void write_stores();
void write_bills();

void add_new_product();
void add_new_store();
void add_new_bill();

bool bill_is_exist(int n);
bool store_is_exist(int n);
bool product_is_exist(int n);
bool store_is_empty(int n);

void read_bills();
void read_product();
void read_stores();

void print_product();
void print_stores();
void print_bills();

void add_product_to_a_store();

void reload_products();

void reload_report();


/////////////////////////////
int compare_dates(date* d_1, date curr)
{
    if(d_1 == NULL)
        return 1;
    if(d_1->y > curr.y)
    {
        return 1;
    }
    else if (d_1->y == curr.y)
    {
        if(d_1->m > curr.m)
        {
            return 1;
        }
        else if(d_1->m == curr.m)
        {
            if(d_1->d >= curr.d)
                return 2;
            else
                return 0;
        }
        else
            return 0;
    }
    else
        return 0;


}

int test_date(date d)
{
    if( 2000 < d.y && d.y <= 2030)
    {
        if ( 0 < d.m  && d.m <= 12)
        {
            if( d.m == 2)
            {
                if( d.y%4 ==0)
                {
                    if(0 < d.d && d.d <= 29)
                    {
                        return 1;
                    }
                    else
                    {
                        cout << "\n\n\tInvalid day number !(must be between 0 and 29)\n";
                        return -1;
                    }
                }
                else
                {
                    if(0 < d.d && d.d <= 28)
                        return 1;
                    else
                    {
                        cout << "\n\n\tInvalid day number !(must be between 1 and 28)\n";
                        return -1;
                    }
                }
            }
            else if (d.m%2 == 1)
            {
                if(0 < d.d && d.d <= 31)
                    return 1;
                else
                {
                    cout << "\n\n\tInvalid day number !(must be between 1 and 31)\n";
                    return -1;
                }
            }
            else
            {
                if(0 < d.d && d.d <= 30)
                    return 1;
                else
                {
                    cout << "\n\n\tInvalid day number !(must be between 1 and 30)\n";
                    return -1;
                }
            }
        }
        else
        {
            cout << "\n\n\tInvalid month number !(must be between 1 and 12)\n";
            return -2;
        }
    }
    else
    {
        cout << "\n\n\tInvalid year number ! (must be between 2000 and 2030)\n";
        return -3;
    }
}

bool test(int n)
{
    if(n >= 0)
        return 1;
    else
        return 0;
}

void delete_product_from_store()
{
    int sn,pn;
    while(1)
    {
        cout << "\n\nEnter store number :  ";
        cin >> sn;
        if(!test(sn))
        {
            cout << "\n\n\t*Please enter a positive number!\n\n";
            continue;
        }
        if(store_is_exist(sn))
                if(!store_is_empty(sn))
                        break;
                else
                    cout << "\n\n\tThis store is empty! please reenter store number\n\t----------------------------------\n";
        else
            cout << "\n\n\tThis store is not exist! please reenter store number.\n\t----------------------------------------\n";
    }
    cout << "``````````````````````````````````";
    s_node* s_temp = s_head;
    while (s_temp!=NULL)
    {
        if(s_temp->sptr->get_num() == sn)
            break;
        s_temp = s_temp->next;
    }
    while(1)
    {
        cout << "\nEnter product number :  ";
        cin >> pn;
        if(!test(pn))
        {
            cout << "\n\n\t*Please enter a positive number!\n\n";
            continue;
        }
        if(product_is_exist(pn))
            if(s_temp->sptr->product_exist(pn))
                break;
            else
                cout << "\n\n\tThis store does not have this product ! please reenter product number.\n\t------------------------------------\n";
        else
            cout << "\n\n\tThis product is not declared ! please reenter product number.\n---------------------------------------------\n";
    }
    cout << "``````````````````````````````````";
    s_temp->sptr->delete_product(pn);
    write_stores();
    cout << "\n---------------PRODUCT HAS BEEN DELETED SUCCESSFULLY! \n\n";
}

void count_sells_cost()
{
    date dat;
    b_node* b_temp = b_head;
    int x ,y=0;
    float average = 0,total=0;
    cout << "\n--------------------------------------------------------\n";
    cout << "\nEnter date :\n";
year:
    cout << "\t\t->year: ";
    cin >> dat.y;
month:
    cout << "\t\t->month: ";
    cin >> dat.m;
    dat.d = 1;
    x = test_date(dat);
    if (x== -3)
    {
        goto year;
    }
    else if(x==-2)
    {
        goto month;
    }
    cout << "\n---------------------------------------------------------------------\n";
    while(b_temp != NULL)
    {
        if(compare_dates(b_temp ->bptr->get_bill_date(),dat)== 2)
        {
            y++;
            total += b_temp->bptr ->get_cost();
        }
        b_temp = b_temp->next;
    }
    cout << "\n\t Total sells in (" << dat.y << "/" << dat.m << ") :  " << total << endl;
    average =  (total/y)*(1.0) ;
    cout << "\n\t Average is :  " << average << endl;
}


void expire_products_report()
{
    read_product();
    p_node* p_temp = p_head;
    date* d;
    cout << "  \n\n-------------EXPIRE PRODUCTS :\n``````````````````````````````````````````````````````````\n";
    while(p_temp != NULL)
    {
        d=p_temp->ptr->get_expiry_date();
        if(compare_dates(d,curr)== 0 )
        {
            cout << "\n\t->"<<p_temp->ptr->get_product_num() << "\t\t//"<< d->d<< "."<< d->m << "." << d->y << "//\n";
        }
        p_temp = p_temp->next;
    }

}

////////////////////////////
bool bill_is_exist(int n)
{
    read_bills();
    b_node* b_temp = b_head;
    while(b_temp!=NULL)
    {
        if(b_temp->bptr->get_bill_num()==n)
            return 1;
        b_temp = b_temp->next;
    }
    return 0;
}

bool product_is_exist(int n)
{
    read_product();
    p_node* p_temp = p_head;
    while(p_temp!=NULL)
    {
        if(p_temp->ptr->get_product_num()==n)
        {
            return 1;
        }
        p_temp = p_temp->next;
    }
    return 0;
}

bool store_is_exist(int n)
{
    read_stores();
    s_node* s_temp = s_head;
    while (s_temp != NULL)
    {
        if(s_temp->sptr->get_num()== n)
            return 1;
        s_temp = s_temp->next;
    }

    return 0;
}

bool store_is_empty(int n)
{
    s_node* s_temp = s_head;
    while(s_temp!=NULL)
    {
        if(s_temp->sptr->get_num() == n)
            break;
        s_temp = s_temp->next;
    }
    if(s_temp->sptr->get_head() == NULL)
        return 1;
    else
        return 0;
}

//////////////////////////
void read_stores()
{
    s_numb =0;
    s_head=NULL;
    string line;
    ifstream sto;
    sto.open("store.txt" , ios::in);
    if(sto.is_open())
    {
        int x;
        string add;
        sto >> x;
        if(!sto.eof())
         {
            while (x==0)
            {
                sto >> x;
            }
         }
        while(!sto.eof())
        {
            sto >> add;
            getline(sto,line);
            s_node* s_temp = new s_node;
            s_temp->next = NULL;
            s_temp->sptr = new store(x,add,NULL);
            s_numb++;
            if(s_head==NULL)
            {
                s_head=s_temp;
                s_head->next=NULL;
            }
            else
            {
                s_node* t = s_head;
                while(t->next!=NULL)
                {
                    t=t->next;
                }
                t->next = s_temp;
                s_temp->next=NULL;
            }
            sto >>  x;
            while(x==0 && !sto.eof())
            {
                p_n* temp = new p_n;
                sto >> temp->num;
                sto >> temp->quantity;
                s_temp->sptr->add_product(temp);
                sto >> x;
            }
        }
    }
}

void read_product()
{
    p_numb=0;
    date d;
    p_head=NULL;
    ifstream pro;
    pro.open("product.txt" , ios::in);
    if(pro.is_open())
    {
        int n,pn,b_price,b_pieces,day,mon,year;
        string name,line;
        char c;
        pro >> n;
        while(!pro.eof())
        {
            p_node*p_new = new p_node;
            p_new->next = NULL;
            pro >> name >>  pn >> b_price >> b_pieces ;
            pro >> c;
            if(c=='*')
            {
               p_new->ptr =new unxpire_product(n,name,pn,b_price,b_pieces);
               getline(pro,line);
               p_numb++;
            }
            else
            {
                day= c - '0';
                pro >> c >> mon >> c >> year ;
                d.d=day;d.m=mon;d.y=year;
                p_new->ptr = new expire_product(d,n,name,pn,b_price,b_pieces);
                p_numb++;
            }
            pro >> n;
            if(p_head==NULL)
            {
                p_head=p_new;
                p_head->next = NULL;
            }
            else
            {
                p_node* temp = p_head;
                while(temp->next!=NULL)
                {
                    temp=temp->next;
                }
                temp->next=p_new;
            }
        }
    }
}

void read_bills()
{
    b_numb = 0;
    b_head = NULL;
    string line;
    ifstream bill_f;
    bill_f.open("bills.txt", ios::in);
    if(bill_f.is_open())
    {
        string name;
        date d;
        char c;
        int bn,sn,x;
        float tot;
        while(!bill_f.eof())
        {
            b_node* b_new = new b_node;
            bill_f >> bn >> name >> sn >> d.d >> c >> d.m >> c >> d.y >> c >> c ;
            if(c == 'c')
            {
                bill_f >> c >> tot >> c >> c >> x;
                b_new->bptr =new cash(tot,bn,sn,name,d,NULL);
            }
            else
            {
                bill_f >> c >> tot >> c >> c >> x;
                b_new->bptr = new prepaid(tot,bn,sn,name,d,NULL);
            }
            bill_f >> c;
            while(c == '*' )
            {
                pnod* pnew = new pnod;
                bill_f >> pnew->pnum >> pnew->box_n >> pnew->price >> x ;
                b_new->bptr->add_product_to_bill(pnew);
                bill_f >> c;
            }
            getline(bill_f,line);
            b_numb ++;
            if(b_head == NULL)
            {
                b_head = b_new;
                b_head ->next = NULL;
            }
            else
            {
                b_node* b_temp = b_head;
                while(b_temp->next != NULL)
                {
                    b_temp = b_temp->next;
                }
                b_temp->next = b_new;
                b_new ->next = NULL;
            }

        }
    }
}

//////////////////////////
void add_new_bill()
{
    char choice;
    date* d;
    int b_num,s_num,x;
    float total = 0;
    date b_d;
    string c_name;
    pnod* phead = new pnod;
    phead = NULL;
    int i,a[100];
    read_product();
    read_stores();
    do
    {
        read_bills();
        b_node* b_new = new b_node;
        b_new->next = NULL;
        cout << "``````````````````````````````````````````\n";
        while(1)
        {
            cout << "\nEnter bill number :";
            cin >> b_num;
            if(!bill_is_exist(b_num))
                break;
            else
                {
                    cout << "\n\n\t*There number is used before!please reenter bill number.\n";
                    cout << "\t -------------------------------------------------------------\n";
                }
        }
        cout << "``````````````````````````````````````````\n";
        cout << "\nEnter date :\n";
year1:
        cout << "\n\t\t->year: ";
        cin >> b_d.y;
month1:
        cout << "\n\t\t->month: ";
        cin >> b_d.m;
day1:
        cout << "\n\t\t->day: ";
        cin >> b_d.d;
        x = test_date(b_d);
        if (x== -3)
        {
            goto year1;
        }
        else if(x==-2)
        {
            goto month1;
        }
        else if(x==-1)
        {
            goto day1;
        }
        if( compare_dates(&b_d , curr))
        {
            cout << "\n\n\tYou can't enter a date that has not happened yet!\n\n";
            goto year1;
        }
        cout << "``````````````````````````````````````````\n";
        cout << "\nCustomer name :  ";
        cin >> c_name;
        i=0;
        cout << "``````````````````````````````````````````\n";
        while(1)
        {
            cout << "\nStore number : ";
            cin >> s_num;
            if(store_is_exist(s_num))
                if(store_is_empty(s_num))
                {
                    cout << "\n\n\t*This store is empty!please choose another number.\n";
                    cout << "\t -----------------------------------------------------\n";
                    continue;
                }
                else
                    break;
            else
            {
                cout << "\n\n\t*There is no store with this number!..please reenter store number\n ";
                cout << "\t ------------------------------------------------------------------\n";
            }
        }
        cout << "``````````````````````````````````````````\n";


product:
        do
        {
            p_node* temp = p_head;
            pnod* pnew = new pnod;
            pnew->next = NULL;
            cout << "\nEnter product number : ";
            cin >> pnew->pnum;
            s_node* s_temp = s_head;
            while(s_temp!=NULL)
            {
                if(s_temp->sptr->get_num()==s_num)
                    break;
                s_temp = s_temp->next;
            }
            while(1)
            {
                if(product_is_exist(pnew->pnum))
                {
                    if(!s_temp->sptr->product_exist(pnew->pnum))
                    {
                        cout << "\n\n\t*This store does not have this product!\n";
                        cout << "\t -------------------------------------------\n\nplease enter another number : ";
                        cin >> pnew->pnum;
                    }
                    else
                        break;
                }
                else
                {
                    cout << "\n\n\t*Product number was not found!\n";
                    cout << "\t -------------------------------------\n\nplease enter another number :";
                    cin >> pnew->pnum;
                }
            }
            for(int j=0;j<i ; j++)
            {
                if(a[j]==pnew->pnum)
                {
                    cout << "\n\n\t*you have entered this product already! please choose another number.\n";
                    cout << "\t -----------------------------------------------------------------------\n";
                    goto product;
                }
            }
            while(temp!=NULL)
            {
                if(temp->ptr->get_product_num() == pnew->pnum)
                    break;
                temp = temp->next;
            }
            d = temp->ptr->get_expiry_date();
            if((compare_dates(d , b_d) == 1) || d == NULL)
            {
                    pnew->price = temp->ptr->get_box_price();
            }
            else if( compare_dates(d , b_d) == 2 )
            {
                pnew->price = (0.5)*temp->ptr->get_box_price();
            }
            else if (s_temp->sptr->get_quantity(pnew->pnum)==0)
            {
                cout << "\n\n\t*There is no boxes of this product in this store ! please choose another number.\n";
                cout << "\t ------------------------------------------------------------------------------------\n";
                goto product;
            }
            else
            {
                cout << "\n\n\t*This product is expired ! please choose another number.\n";
                cout << "\t -------------------------------------------------------------\n";
                goto product;
            }
            a[i] = pnew->pnum;
            cout << "``````````````````````````````````````````\n";
            cout << "\nEnter box quantity :  ";
            cin >> pnew->box_n;
            while(s_temp->sptr->get_quantity(pnew->pnum) < pnew->box_n)
            {
                cout << "\n\n\t*Product quantity is not enough!\n ";
                cout << "\t -------------------------------------\n\n please enter less quantity: ";
                cin >> pnew->box_n;
            }
            s_temp->sptr->quantity_add(pnew->pnum , -(pnew->box_n));
            cout << "before";
            write_stores();
            cout << "after";
            if(phead == NULL)
            {
                cout << "first";
                phead =pnew;
                phead->next = NULL;
            }
            else if ((phead->pnum) > (pnew->pnum))
            {
                cout << "head";
                pnew->next = phead;
                phead = pnew;
            }
            else
            {
                cout << "2";
                pnod* prev = phead;
                pnod* cur = phead->next;
                while((cur!=NULL) && (cur->pnum < pnew->pnum))
                {
                    prev = prev->next;
                    cur = cur->next;
                }
                pnew->next = cur;
                prev->next = pnew;
            }
            i++;
            cout << "\n  ->(y) to add another product.\n  ->(any other key) to quit adding.\n\t\t#your choice: ";
            cin >> choice;
        }while(choice == 'y');
        cout << "0";
        pnod* ptemp = phead;
        while(ptemp!=NULL)
        {
            cout << "**" << ptemp->pnum;

            total += (ptemp->price)*(ptemp->box_n);
            ptemp = ptemp->next;
        }
        cout << "``````````````````````````````````````````\n";
again:
        cout << "\n Cash ->(c)\n Prepaid ->(p)\n\t\t#your choice :  ";
        cin >> choice;
        if(choice == 'c')
        {
            b_new->bptr = new cash(total,b_num,s_num,c_name,b_d,phead);
        }
        else if(choice == 'p')
        {
            b_new->bptr = new prepaid(total,b_num,s_num,c_name,b_d,phead);
        }
        else
        {
            cout << "\n\n\t*Invalid character!please reenter your choice.";
            cout << "\t  -------------------------------------------------------------\n";
            goto again;
        }
        if(b_head == NULL)
        {
            b_head = b_new;
            b_head->next = NULL;
        }
        else
        {
            b_node* b_temp = b_head;
            while(b_temp->next != NULL )
            {
                b_temp = b_temp->next;
            }
            b_temp ->next =b_new;
            b_new->next = NULL;
        }
        write_bills();
        cout << "\n---------------------------BILL HAS BEEN ADDED SUCCESSFULLY!\n\n";
        cout << "Enter:\n ->(y) to add another bill.\n ->(any other key) to quit adding.\n\t\t#your choice: ";
        cin >> choice;
    }while(choice == 'y');
    cout << "-------------------------------------------------------";
}

void add_new_store()
{
    char choice;
    int n,a[p_numb],i=0;
    string add;
    char c;
    do
      {
        read_stores();
        s_node* s_new = new s_node;
        s_new->next=NULL;
        p_n* phead= new p_n;
        phead = NULL;
        cout << "\n\nEnter store number: ";
        cin >> n;
        while(store_is_exist(n))
        {
            cout << "\n\t *This number is already exist!\n\nPlease enter another number:";
            cout << "\t------------------------------------------------------------------\n";
            cin >> n ;
        }
        cout << "\n\nEnter store address : ";
        cin >> add;
        cout << "\nEnter :\n\t ->(e) If the store is empty.\n\t ->(any key else)If not.\n\t\t#your choice: ";
        cin >> c;
        if(c=='e')
        {
            phead = NULL;
            goto Fin;
        }
        while(1)
        {
            p_n* temp = new p_n;
            while(1)
            {
                cout << "\n\nproduct number : " ;
                cin >> temp->num;
                a[i]=temp->num;
                for(int j=0;j<i;j++)
                {
                    if(a[j]==temp->num)
                        c = 'n';
                }
                if(product_is_exist(temp->num))
                {
                    if(c=='n')
                    {
                        cout << "\n\t*You have used this product already!\n";
                        cout << "\t-----------------------------------------\n";
                        c='y';
                    }
                    else
                    {
                        break;

                    }
                }
                else
                cout <<  "\n\t\t*there is no product with this number!*\n\nplease enter another  ";
            }

            cout<< "\nproduct quantity :" ;
            cin >> temp->quantity ;
            i++;
            if(phead==NULL)
                {
                    phead=temp;
                    phead->next=NULL;
                }
            else if( (phead->num) > (temp->num))
            {
                temp->next =phead ;
                phead =temp;
            }
            else
            {
                p_n* prev=phead ;
                p_n* curr=phead->next;
                while((curr!= NULL) && curr->num < temp->num)
                {
                    prev=curr;
                    curr=curr->next;
                }
                temp->next=curr;
                prev->next = temp;
            }
            cout << "\nenter any key to add another product.\nenter (q)to quit adding ";
            cin >> c;
            if (c=='q')
                break;
    }
    Fin:
        s_new->sptr = new store(n,add,phead);
        s_numb++;
        if(s_head==NULL)
        {
            s_head=s_new;
            s_head->next=NULL;
        }
        else
        {
            s_node* s_temp = s_head;
            while(s_temp->next!=NULL)
            {
                s_temp=s_temp->next;
            }
            s_temp->next=s_new;
            s_new->next=NULL;
        }
        write_stores();
        i = 0;
        cout << "add another store? (Y/N)";
        cin >> choice ;
      }while(choice=='y');
}

void add_new_product()
{
    date dat;
    char c,choice;
    int n,l_number,b_p,b_ps;
    string name;
    do
    {
        read_product();
        p_node* p_new = new p_node;
        p_new->next=NULL;
        while(1)
        {
            cout << "\nEnter product number : ";
            cin >> n;
            if(!product_is_exist(n))
                break;
            cout << "\n\n\t\t*This number is already exist!\n";
        }
        cout << "\nName of the new product:   ";
        cin >> name;
        cout << "\nproduction   number    :    ";
        cin >> l_number;
        cout << "\nBox price       :   ";
        cin >> b_p;
        cout << "\nBox pieces     :   ";
        cin >> b_ps;
        cout << "\n Does the product have an expiry date?\n yes->(y) \nNo->(n)\n\nyour choice:  ";
        cin >> choice;
        if(choice!='y')
        {
             p_new->ptr = new unxpire_product(n,name,l_number,b_p,b_ps);
        }
        else
        {
            cout << "\nExpire date: \n ->d: ";
            cin >> dat.d;
            cout << " ->m: ";
            cin >> dat.m;
            cout << " ->y: ";
            cin >> dat.y;
            p_new->ptr = new expire_product(dat,n,name,l_number,b_p,b_ps);
        }
        if(p_head==NULL)
        {
            p_head=p_new;
            p_head->next = NULL;
        }
        else
        {
            p_node* temp = p_head;
            while(temp->next!=NULL)
            {
                temp=temp->next;
            }
            temp->next=p_new;

        }
        write_product();
        cout << "\nEnter\n(y) to add another product.\n(any other key) to quit adding.";
        cin >> c;
    }while(c=='y' || c== 'Y');

    cout << "\n\n\t\tPRODUCTS HAS BEEN ADDED SUCCESSFULLY.\n************************************************\n\n";

}
//////////////////////////////
void print_stores()
{
    read_stores();
    if(s_head==NULL)
        cout << "\n\t\t*store file is empty*\n\n";
    else
     {
         cout << "\n\n";
        s_node* s_temp = s_head;
        while(s_temp != NULL)
        {
            s_temp->sptr->print_s();
            s_temp = s_temp->next;
            cout << "\n";
        }
     }
}

void print_product()
{
    read_product();
    p_node* temp = new p_node ;
    temp = p_head;
    if(p_head == NULL)
    {
        cout << "\n\t\t *Product file is empty!\t\n";
    }
    else
    cout << "\n================================================";
    cout << "\n   //Food & Detergents //:\n==========================================================================\n";
    cout <<                      "||number|| Name ||Production|| Box pieces || Box price || expiry date  ||\n";
    cout << "------------------------------------------------------------------------\n";
    while(temp!=NULL)
    {
        if(temp->ptr->get_expiry_date()!=NULL)
        {
            temp->ptr->print();
            cout << "\n------------------------------------------------------------------------\n";
        }
        temp = temp->next;
    }
    temp = p_head;
    cout << "\n\n================================================\n";
    cout << "   //House pots// :\n================================================================================\n";
    cout <<                      "||number|| Name ||Production|| Box pieces || Box price ||\n";
    cout << "------------------------------------------------------------------\n";
    while(temp!=NULL)
    {
        if(temp->ptr->get_expiry_date()==NULL)
        {
            temp->ptr->print();
            cout << "\n------------------------------------------------------------------\n";
        }
        temp = temp->next;
    }
}

void print_bills()
{
    b_node* b_temp = b_head;
    if(b_head == NULL)
    {
        cout << "\n\n\t*Bills file is empty!\n";
    }
    else
     {
        while(b_temp!= NULL)
        {
            cout << "-------------------------------------------------------------------------------\n|";
            cout << "\n| Number | Costumer |Store |   Date   | C/P |\t\t\t\t      \n";
            cout <<   "|````````````````````````````````````````````                                 \n";
            b_temp->bptr->print_bill();
            b_temp = b_temp->next;
        }

    }

}
/////////////////////////////
void write_bills()
{
    ofstream bill_f;
    bill_f.open("bills.txt" , ios::trunc);
    bill_f.close();
    b_node* b_temp = b_head;
    while(b_temp != NULL)
    {
        cout << "*";
        b_temp->bptr->write_bill_on_file();
        b_temp = b_temp->next;
    }
}

void write_stores()
{
    s_node* temp=s_head;
    ofstream sto;
    sto.open("store.txt" , ios::trunc);
    sto.close();
    while(temp!=NULL)
    {
        temp->sptr->write_on_file();
        temp = temp->next;
    }
}

void write_product()
{
    p_node* temp = p_head;
    ofstream pro;
    pro.open("product.txt" , ios::trunc);
    pro.close();
    while(temp!=NULL)
    {
        temp->ptr->write_product_on_file();
        temp = temp->next;
    }
}
////////////////////////////////

void add_product_to_a_store()
{
    read_stores();
    int number,s_number,q;
    char choice;
    p_n* new_product;

    cout << "\nEnter store number :  ";
    cin >> s_number;
    while(!store_is_exist(s_number))
    {
        cout << "\n\n\t*There is no store with this number.\n\n ->(y) add it to the store file.\n ->(any other key) Choose another number.\n";
        cout << "\t#your choice :  ";
        cin >> choice;
    if(choice=='y')
    {
        add_new_store();
    }
    else
    {
        cout << "\nEnter store number :  ";
        cin >> s_number;
    }
    }
retry:
     cout << "\nEnter product number :  ";
    cin >> number;
    while(!product_is_exist(number))
    {

        cout << "\n\n\t*There is no product with the number you have entered! \n\n ->(Y) add it to the products file.\n ->(any other key) enter another number.\n";
        cout << "\t#your choice :  ";
        cin >> choice;
        if(choice=='y')
        {
            add_new_product();
        }
        else
        {
            cout << "\nEnter product number :  ";
            cin >> number;
        }
    }
    s_node* s_temp = s_head;
    while(s_temp!=NULL)
    {
        if(s_temp->sptr->get_num()==s_number)
        {
            break;
        }
        s_temp = s_temp->next;
    }
    if(s_temp->sptr->product_exist(number))
    {
        cout << "\n\n\t*This product is already exist in this store!please enter another number.\n";
        goto retry;
    }
    cout << "\nEnter quantity:  ";
    cin >> q;
    new_product = new p_n;
    new_product->num=number;
    new_product->quantity=q;
    new_product->next=NULL;

    s_temp->sptr->add_product(new_product);
    write_stores();
    cout << "\n\n\tPRODUCT HAS BEEN ADDED TO THE STORE NUMBER (" << s_number << ") SUCCESSFULLY!\n\t\t***************************************************\n";
}

////////////////////////////////
void reload_products()
{
    int pn, sn,q;
    char c;
    start: cout << "\n\n*Enter store you want to reload its products:  ";
    cin >> sn;
    while(1)
    {
        if(store_is_exist(sn))
            if(store_is_empty(sn))
                {
                    cout << "\n\t*store is empty! Please choose another store.\n\n";
                    cout << "Store number : ";
                    cin >> sn;
                }
            else
                break;
        else
        {
            cout << "\n\n\t*This store is not exist !\n\n";
            cout << "reenter store number : ";
            cin >> sn;
        }
    }
    while(1)
    {
        s_node* s_temp = s_head;
        while ( s_temp != NULL)
        {
            if (s_temp->sptr->get_num() == sn)
            {
                cout << "\n\n*Enter product number :   ";
                cin >> pn;
                while(!s_temp->sptr->product_exist(pn))
                {
                    cout << "\n\n\t*The product was not found !\n\n  ->(y)Enter enter another number.\n  ->(q)to quit adding.\n  ->(any other key)to restart the process.\n";
                    cout << "\t#your choice :  ";
                    cin >> c;
                    if(c == 'y')
                    {
                        cout << "\nproduct number : ";
                        cin >> pn;
                    }
                    else if (c==q)
                        goto End;
                    else
                        goto start;
                }
                if(!s_temp->sptr->need_to_reload(pn))
                {
                    cout << "\n\n\t*This product does not need to be reloaded!\n";
                    cout << "\t---------------------------------------------------\n ->(y) to enter another number.\n ->(any other key) to continue adding.\n";
                    cout << "\t\t#your choice :  ";
                    cin >> c;
                    if(c == 'y')
                    {
                        goto start;
                    }
                }
                cout << "\n Enter additional quantity:  ";
                cin >> q;
                while(!s_temp->sptr->quantity_add(pn,q))
                {
                    cout << "\n *This quantity is not enough! please add more.\n\nquantity : ";
                    cin >> q;
                }
                write_stores();
                goto End;

            }
            s_temp = s_temp->next;
        }
    }
 End:;

}

void reload_report()
{
    read_stores();
    int c=0,t=0;
    s_node* s_temp = s_head;
    cout << "  \n\n PRODUCTS THAT NEED TO RELOAD (less than 50 boxes) ARE :\n\n";
    while ( s_temp != NULL)
    {
        c = s_temp->sptr->reload_products();
        if(c==1)
        {
            t=1;
        }
        s_temp = s_temp->next;
    }
    if (t==0)
        cout << "\n\n\t*there is no products need to reload!";

}
////////////////////////////////

int main()
{
     read_product();
        //read_stores();
        //read_bills();
        int c,x;
        cout << "---------------------------------------------------------------------\n";
        cout << "---------------------------------------------------------------------\n";
        cout << "\n Please enter current date :\n ```````````````````````````\n";
year:
        cout << "\t\t->year: ";
        cin >> curr.y;
month:
        cout << "\t\t->month: ";
        cin >> curr.m;
day:
        cout << "\t\t->day: ";
        cin >> curr.d;
        x = test_date(curr);
        if (x== -3)
        {
            goto year;
        }
        else if(x==-2)
        {
            goto month;
        }
        else if(x==-1)
        {
            goto day;
        }

        cout << "\n---------------------------------------------------------------------\n";
        while(1)
         {
            cout << "\n ->(1) show products .\n" << " ->(2) show stores.\n" << " ->(3) show bills.\n ->(4) add product to a store.\n";
            cout << " ->(5) reload report.\n" <<" ->(6) add a quantity to a product .\n ->(7) expiry report.\n ->(8) count products sells and average during a month.\n";
            cout << " ->(9) delete a product from a store.\n ->(10) add a new product.\n ->(11) add a new stores.\n ->(12) add a new bill.\n ->(13) Exit." ;
            cout << "\n\n\t  #your choice : ";
            cin >> c;
            switch(c)
            {
            case 1:
                print_product();
                break;
            case 2:
                print_stores();
                break;
            case 3:
                print_bills();
                break;
            case 4:
                add_product_to_a_store();
                break;
            case 5:
                reload_report();
                break;
            case 6:
                reload_products();
                break;
            case 7:
                expire_products_report();
                break;
            case 8:
                count_sells_cost();
                break;
            case 9:
                delete_product_from_store();
                break;
            case 10:
                add_new_product();
                break;
            case 11:
                add_new_store();
                break;
            case 12:
                add_new_bill();
                break;
            case 13:
                return 0;
            default:
                cout << "\n\n\t*Invalid choice!please enter another number.\n";
                cout << "\t---------------------------------------------------\n";
                break;
            }
         }
}
