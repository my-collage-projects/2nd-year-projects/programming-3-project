#include "prepaid.h"
#include<iostream>
#include<fstream>
using namespace std;

prepaid::prepaid()
{
    after_discount =total;
}

prepaid::prepaid(float t,int b_n,int s_n,string name,date b_d,pnod* phead) : bill(t,b_n, s_n , name , b_d , phead)
{
    if(total > 5000)
    {
        after_discount = total - (0.05 + 0.1 )*total;
    }
    else
        after_discount = total - (0.1)* total;
}

void prepaid::write_bill_on_file()
{
    bill::write_bill_on_file();
    ofstream bill_file;
    bill_file.open("bills.txt" , ios::app);
    if(head == NULL)
    {
        bill_file << "-------------------------------------------------\n";
    }
    else
    {
        bill_file << "(p)" << total << "->" << after_discount << "\n";
        pnod* temp = head;
        while(temp!=NULL)
        {
            bill_file << "*\t\t\t" << temp->pnum << "  " << temp->box_n << "  " << temp->price << "  " <<(temp->box_n)*(temp->price) << "  \n" ;
            temp = temp->next;
        }
        bill_file << "-------------------------------------------------";
    }
    bill_file.close();
}

int prepaid::get_cost()
{
    return after_discount;
}

void prepaid::print_bill()
{
    bill::print_bill();
    cout << "  . P .\t\t\t\t\t|\n|\t\t\t\t\t\t || Total|| Discounted||      \n";
    if(head == NULL)
    {
        cout << "-------------------------------------------------------------------------------\n";
    }
    else
    {
        cout << "||P_NUM  |  PRICE  |  QUANTITY  |  COST  |\t  .  " <<total <<"  .    "<< after_discount<<"  .\n";
        cout << "|```````````````````````````````````````````` \n";

        pnod* ptemp=head;
        while (ptemp!=NULL)
        {
            cout << "|    " << ptemp->pnum << "    .   " << ptemp->price << "    .     "<< ptemp->box_n  << "      .   "<< (ptemp->box_n)*(ptemp->price)<<" \n";
            ptemp = ptemp->next;
        }
        cout << "-------------------------------------------------------------------------------\n\n";
    }
}

prepaid::~prepaid()
{

}
