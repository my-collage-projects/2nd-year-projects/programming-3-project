#include "store.h"
#include<fstream>
#include <iostream>
using namespace std;

store::store()
{
    s_num=0;
    s_add= "";
    head=NULL;
}

store::store(int n,string add,p_n* temp)
{
    s_num = n;
    s_add =add;
    head = temp;
}

void store::print_s()
{
    cout << "\n------------------------------------------------\n";
    cout << "store ***  " << s_num << "  ***\n";
    cout << "------------------------------------------------\n";
    cout << "|store number||store address|";
        if(head==NULL)
        {
            cout << "\t//no products in this store//\n" << "|      " <<s_num << "     ||   " << s_add << "|"<< endl ;
        }
        else
        {
            cout << "\t  ||product number||  quantity  ||\n";
             cout << "|      " <<s_num << "     ||     " << s_add << "   |   ";
            p_n* temp=head;
            while(temp!=NULL)
            {
                cout << "  |       " << temp->num << "       ||      " << temp->quantity <<"     |\n\t\t\t\t";
                temp=temp->next;
            }
        }
}

int store::need_to_reload(int n)
{
    p_n* temp = head;
    while(temp != NULL)
    {
        if(temp->num == n)
        {
            if(temp->quantity <= 50)
                return 1;
            else
                return 0;
        }
        temp = temp->next;
    }
    return 0;


}

int store::reload_products()
{

    if(head == NULL)
        return 0;
    else
    {
        int c=0;
        p_n * temp;
        temp = head;
        while(temp!=NULL)
        {
            if(temp->quantity < 50)
            {
                c=1;
                break;
            }
            temp = temp->next;
        }
        if(c==0)
        {
            return 0;
        }
        else
        {
            cout << "****************store number (" <<  s_num << "):\n";
            temp = head;
            while(temp!=NULL)
            {
                if(temp ->quantity < 50)
                    cout << "\t\t\t\t\t--->" << temp->num << "\n";
                temp = temp->next;
            }
            cout << "_________________________________________________________\n";
            return 1;
        }
    }
}

bool store::product_exist(int n)
{
    p_n* temp = head;
    while (temp!=NULL)
    {
        if(temp->num == n)
            return 1;
        temp = temp->next;
    }
    return 0;
}

int store::get_quantity(int n)
{
    p_n* temp = head;
    while(temp!=NULL)
    {
        if(temp->num == n)
            return temp->quantity;
        temp = temp->next;
    }
    return 0;
}

int store::quantity_add( int n, int m)
{
    p_n* temp = head ;
    while (temp != NULL)
    {
        if (temp->num == n)
        {
            temp->quantity = temp->quantity + m;
            break;
        }
        temp = temp->next;
    }
    if(temp->quantity > 50)
        return 1;
    else
        return 0;
}

int store :: get_num()
{
    return s_num;
}

p_n* store::get_head()
{
    return head;
}

void store::add_product(p_n* p)
{
    if(head==NULL)
        {
            head=p;
            head->next= NULL;
        }
    else if( (head->num) > (p->num))
    {
        p->next =head ;
        head =p;
    }
    else
    {
        p_n* prev=head ;
        p_n* curr=head->next;
        while((curr!= NULL) && curr->num < p->num)
        {
            prev=curr;
            curr=curr->next;
        }
        p->next=curr;
        prev->next = p;
    }
}

void store::delete_product(int n)
{
    if(head->num == n)
    {
        delete head;
        head = head ->next;

    }
    else
    {
        p_n* prev = head;
        p_n* curr = head->next;
        while(curr != NULL)
        {
            if(curr->num == n)
                break;
            curr = curr->next;
            prev = prev->next;
        }
        prev->next = curr->next;
        delete curr;
    }
}

void store::write_on_file()
{
    ofstream store_file;
    store_file.open("store.txt", ios::app);
    if(store_file.is_open())
    {
        store_file << s_num << " " << s_add;
        if(head == NULL)
        {
            store_file << "\t*no products in this store*\n" ;
        }
        else
        {
            p_n* temp= new p_n;
            temp = head;
            store_file << " \t\t\t******\n";
            while(temp!=NULL)
            {
                store_file << "0\t\t\t" << temp->num << "  " << temp->quantity <<"\n";
                temp=temp->next;
            }
        }
        store_file.close();
    }
    else
        cout << "\n\t*Error opening file (store.txt)!\n ";

}

store::~store()
{
    delete &s_num;
    delete &s_add;
    while(head!=NULL)
    {
        delete head;
        head = head->next;
    }

}
