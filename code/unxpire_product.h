#ifndef UNXPIRE_PRODUCT_H
#define UNXPIRE_PRODUCT_H
#include "product.h"


class unxpire_product : public product
{
    private:
    public:
        unxpire_product();
        unxpire_product(int n,string name,int pn , int b_price , int b_pieces);
        ~unxpire_product();
        date* get_expiry_date();
        void print();
        void write_product_on_file();
};

#endif // UNXPIRE_PRODUCT_H
