#ifndef BILL_H
#define BILL_H

#include <iostream>
#include <string.h>
using namespace std;
#include"store.h"

struct pnod
{
    int pnum,box_n,price;
    pnod* next;
};

class bill
{
    protected:
        int s_num,b_num;
        float total , after_discount;
        date b_date;
        string c_name;
        pnod* head;
    public:
        bill();
        bill(float t,int b_n,int s_n,string name,date b_d,pnod* phead);
        ~bill();
        date* get_bill_date();
        virtual int get_cost();
        virtual void print_bill();
        int get_bill_num();
        virtual void write_bill_on_file();
        void add_product_to_bill(pnod* pnew);

};

#endif
