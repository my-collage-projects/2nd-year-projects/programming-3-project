#include "cash.h"
#include"bill.h"
#include<iostream>
#include<string.h>
using namespace std;
#include<fstream>

cash::cash()
{

}

cash::cash(float t,int b_n,int s_n,string name,date b_d,pnod* phead) : bill(t,b_n , s_n , name , b_d , phead)
{
    if(total > 7000)
    {
        after_discount = total - (0.05)*total;
    }
    else after_discount = total;
}

void cash::write_bill_on_file()
{
    bill::write_bill_on_file();
    ofstream bill_file;
    bill_file.open("bills.txt" , ios::app);
    if(head == NULL)
    {
        bill_file << "-------------------------------------------------\n";
    }
    else
    {
        bill_file << "(c)" << total << "->" << after_discount << "\n";
        pnod* temp = head;
        while(temp!=NULL)
        {
            bill_file << "*\t\t\t" << temp->pnum << "  " << temp->box_n << "  " << temp->price << "  " <<(temp->box_n)*(temp->price) << "  \n" ;
            temp = temp->next;
        }
        bill_file << "-------------------------------------------------";
    }
    bill_file.close();
}


int cash::get_cost()
{
    return after_discount;
}

void cash::print_bill()
{
    bill::print_bill();
    cout << "  . C .\t\t\t\t\t|\n|````````````````````````````````````````````    || Total|| Discounted||      \n";
    if(head == NULL)
    {
        cout << "-------------------------------------------------------------------------------\n";
    }
    else
    {
        cout << "||P_NUM  |  PRICE  |  QUANTITY  |  COST  |\t  .  " <<total <<"  .    "<< after_discount<<"  .\n";
        cout << "|```````````````````````````````````````````` \n";

        pnod* ptemp=head;
        while (ptemp!=NULL)
        {
            cout << "|    " << ptemp->pnum << "    .   " << ptemp->price << "    .     "<< ptemp->box_n  << "      .   "<< (ptemp->box_n)*(ptemp->price)<<" \n";
            ptemp = ptemp->next;
        }
        cout << "-------------------------------------------------------------------------------\n\n";
    }
}

cash::~cash()
{

}
